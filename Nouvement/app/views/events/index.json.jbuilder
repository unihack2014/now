json.array!(@events) do |event|
  json.extract! event, :id, :date, :time, :message, :location, :state
  json.url event_url(event, format: :json)
end
