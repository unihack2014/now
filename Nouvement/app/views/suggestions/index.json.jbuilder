json.array!(@suggestions) do |suggestion|
  json.extract! suggestion, :id, :num_votes
  json.url suggestion_url(suggestion, format: :json)
end
