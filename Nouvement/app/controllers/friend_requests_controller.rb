class FriendRequestsController < ApplicationController
  before_action :set_friend_request, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /friend_requests
  # GET /friend_requests.json
  def index
    @friend_requests = FriendRequest.where(receiver_id: current_user.id)
  end

  # GET /friend_requests/1
  # GET /friend_requests/1.json
  def show
  end

  # GET /friend_requests/new
  def new
    @friend_request = FriendRequest.new
  end

  # GET /friend_requests/1/edit
  def edit
  end

  # POST /friend_requests
  # POST /friend_requests.json
  def create
    @friend_request = FriendRequest.new(friend_request_params)

    @u = User.find_by(email: params[:friend_request][:email])
    
    if @u != nil
      current_user.sent_friend_requests << @friend_request
      @u.received_friend_requests << @friend_request
    else
      redirect_to @friend_request, notice: 'Email not found'
    end

    respond_to do |format|
      if @friend_request.save
        format.html { redirect_to @friend_request, notice: 'Friend request was successfully created.' }
        format.json { render :show, status: :created, location: @friend_request }
      else
        format.html { render :new }
        format.json { render json: @friend_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /friend_requests/1
  # PATCH/PUT /friend_requests/1.json
  def update
    respond_to do |format|
      if @friend_request.update(friend_request_params)
        format.html { redirect_to @friend_request, notice: 'Friend request was successfully updated.' }
        format.json { render :show, status: :ok, location: @friend_request }
      else
        format.html { render :edit }
        format.json { render json: @friend_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /friend_requests/1
  # DELETE /friend_requests/1.json
  def destroy
    @friend_request.destroy
    respond_to do |format|
      format.html { redirect_to friend_requests_url, notice: 'Friend request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def accept
    @friend_request = FriendRequest.find_by(id: params[:request_id])
    # @friend_request.sender.friends << current_user
    # current_user.friends << @friend_request.receiver
    # @friend_request.destroy
    if params[:request_id] = nil
      redirect_to 'friend_requests/index'
    else
      redirect_to root_path
    end

  end

  def reject
    @friend_request = FriendRequest.find_by(id: params[:request_id])
    @friend_request.destroy
    redirect_to 'friend_requests/index'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_friend_request
      @friend_request = FriendRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def friend_request_params
      params.require(:friend_request).permit(:message)
    end
end
