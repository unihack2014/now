require 'json'
class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :suggest, :vote]
  before_action :authenticate_user!, only: [:index,:show,:new,:edit,:create,:update,:destroy]

  # GET /events
  # GET /events.json
  def index
    @accepted_events = current_user.accepted_events
    @invited_events = current_user.invited_events
    File.open("all_profiles.json", "w") {Event.all.collect { |p| { :location => p.location } }.to_json }
  end

  # GET /events/1
  # GET /events/1.json
  def show
    #if @event in current_user.invited_events || @event in current_user.accepted_events

    #else
    #  redirect_to root_path
    #end
  end

  # GET /events/new
  def new
    @event = Event.new
    #@users = User.search(params[:search])
    @users= current_user.friends
    @location = Location.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.state = params[:state]

    @user_ids = params[:user_ids]

    @event.organiser = current_user

    if params[:allowSuggestion] == '1'
      @event.allowsuggest = 1
    else
      @event.allowsuggest = 0
    end

    respond_to do |format|
      if @event.save
        
        if @user_ids!=nil
          @user_ids.each do |user_id|
            @invited_user = User.find_by(id: user_id)
            @invited_user.invited_events << @event
            AppMailer.new_invited_event(@event,@invited_user).deliver
            # Tom: add relationship between invited users and the event
            @n= Notification.create(content:"New Event created on "+@event.date.to_s+" at "+@event.time.to_s, readstate: 0)
            @invited_user.notifications << @n
            @n.event = @event
            @n.save
          end
        end

        @location_name = @event.location
        @loc = Location.find_by(name: @location_name)
        if @loc == nil
          @loc = Location.new(name: @location_name)
          @loc.save
        end

        @event.invited_users << current_user
        if @event.state == :final
          @event.final_location = @loc
          @event.save
        else
          @suggestion = Suggestion.create(location: @loc, event: @event, num_votes: 0)
        end

        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end


  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def suggest
    @location_name = params[:suggestion][:location]
    @loc = Location.find_by(name: @location_name)
    if @loc == nil
      @loc = Location.new(name: @location_name)
      @loc.save
    end

    @suggestion = Suggestion.create(location: @loc, event: @event, num_votes: 0)
    redirect_to @event
  end

  def vote
    @loc = Location.find_by(id:params['voted_location'])
    @s = Suggestion.find_by(event: @event, location: @loc)

    @s.num_votes += 1

    @s.save

    @event.voters << current_user
    
    redirect_to @event

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:date, :time, :message, :location, :state)
    end
end
