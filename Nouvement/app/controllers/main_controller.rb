class MainController < ApplicationController
  before_action :authenticate_user!
  
  def main
  end

  def create
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

end

