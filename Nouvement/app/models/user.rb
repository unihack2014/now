class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

         has_many :organising_events, class_name: "Event", foreign_key: "organiser_id"
         has_and_belongs_to_many :invited_events, class_name: "Event", join_table: "invited_users_events"
         has_and_belongs_to_many :accepted_events, class_name: "Event", join_table: "accepted_users_events"
         has_and_belongs_to_many :rejected_events, class_name: "Event", join_table: "rejected_users_events"
         has_many :check_ins

         has_many :notifications

         has_many :sent_friend_requests, class_name: "FriendRequest", foreign_key: "sender_id"
         has_many :received_friend_requests, class_name: "FriendRequest", foreign_key: "receiver_id"

         has_many :friends, class_name: "User", foreign_key: "friend_id"

  def self.search(search)

  end

  def full_name
    return first_name + " " + last_name
  end

end	
