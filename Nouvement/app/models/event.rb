class Event < ActiveRecord::Base
	enum state: [ :final, :draft ]
	belongs_to :organiser, class_name: "User"
	has_and_belongs_to_many :invited_users, class_name: "User", join_table: "invited_users_events"
	has_and_belongs_to_many :accepted_users, class_name: "User", join_table: "accepted_users_events"
	has_and_belongs_to_many :rejected_users, class_name: "User", join_table: "rejected_users_events"

	has_many :suggestions, foreign_key: 'event_id'
	has_many :suggested_locations, through: :suggestions

	has_one :final_location, class_name: "Location", foreign_key: "final_event_id"
	has_many :voters, class_name: "User", foreign_key: "voting_event_id"
end
