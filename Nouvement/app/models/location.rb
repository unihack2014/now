class Location < ActiveRecord::Base
	has_many :check_ins

	has_many :suggestions, foreign_key: 'location_id'
	has_many :draft_events, through: :suggestions
end
