class AppMailer < ActionMailer::Base
  default :from => "club.biz.2014@gmail.com"

  	def new_invited_event(event,user)
		@event=event
		@user=user
    	mail(:to => user.email, :subject => "You got a new invited event!")
  	end


end
