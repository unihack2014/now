$(document).ready(function() {
	$(document).ready(function(){
        $("[data-toggle='tooltip']").tooltip();
    });

	$("#acc_set").click(function() {
		$("#panel2").animate({right:"-45%"}, 'slow');
		$("#panel1").animate({right:"1px"}, 'slow');
		$("#map-canvas").animate({right:"45%"}, 'slow');
		$(".navbar navbar-default").animate({right:"45%"}, 'slow');
		$("#panel1").attr("src", "../users/edit");

	});

	$("#location").focusout(function () {
		 $("#search_text", top.document).val($(this).val()); 
		 findPlace();
	});


	$("#create_event").click(function() {
		$("#panel2").animate({right:"-45%"}, 'slow');
		$("#panel1").animate({right:"1px"}, 'slow');
		$("#map-canvas").animate({right:"45%"}, 'slow');
		$("#panel1").attr("src", "../events/new");
	});

	$("#my_event").click(function() {
		$("#panel2").animate({right:"-45%"}, 'slow');
		$("#panel1").animate({right:"1px"}, 'slow');
		$("#map-canvas").animate({right:"45%"}, 'slow');
		$("#panel1").attr("src", "../events");
	});

	$("#search_text").focus(function() {
		$("#panel1").animate({right:"-45%"}, 'slow');
		$("#map-canvas").animate({right:"0px"}, 'slow');
		$("#panel2").animate({right:"-45%"}, 'slow');
	});

	$("#panel2").dblclick(function() {
		$("#map-canvas").animate({right:"0px"}, 'slow');
		$("#panel2").animate({right:"-300px"}, 'slow');
	});

	$("#directions").click(function() {
		$("#panel1").animate({right:"-45%"}, 'slow');
		$("#panel2").animate({right:"1px"}, 'slow');
		$("#map-canvas").animate({right:"45%"}, 'slow');
	});
});