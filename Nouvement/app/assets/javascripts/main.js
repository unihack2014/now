var map;
var markers = [];
var myPlace;
var directions;

var clearMarkers = function() {
  	for (var i in markers) {
  		markers[i].setMap(null);
  	}
};

var clearDirections = function() {
	if (directions != null) {
		directions.setMap(null);
		directions.setPanel(null);
	}
};

var findPlace = function () {
  	clearMarkers();
  	var dest = document.getElementById("search_text").value;
  	var geocoder = new google.maps.Geocoder();
    var request = {
      address: dest
    };
    var bounds = new google.maps.LatLngBounds();
    
	geocoder.geocode(request, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {

			if (results.length == 1) {
				var marker = new google.maps.Marker({
        				map:map,
            			position:results[0].geometry.location
        			});
				
				markers.push(marker);
				map.panTo(marker.getPosition());

			} else {
    				
    			for (var i=0; i < results.length; i++) {
        			var marker = new google.maps.Marker({
        				map:map,
            			position:results[i].geometry.location
        			});
            			

        			bounds.extend(marker.getPosition());   
        			markers.push(marker);
    			}	   
    			map.fitBounds(bounds);
    		}
        
		} else {
        	window.console.log('failed to geocode address: '  + status);
		}
	});
};

var makeDirection = function () {
  	clearDirections();
  	var dest = document.getElementById("search_text").value;
  	var directionsRenderer = new google.maps.DirectionsRenderer();
  	 var directionsService = new google.maps.DirectionsService();
    directionsRenderer.setMap(map);
    directionsRenderer.setPanel(document.getElementById('panel2'));

    // Tom : My added code for changing options to travel
    var selectedMode = document.getElementById('mode').value;


    // Driving directions between Pyrmont and Canberra.
    var request = {
      origin: myPlace,
      destination: dest,
      travelMode: google.maps.TravelMode[selectedMode]
    };

    // Makes a request to the directions server and sets the result on the
    // DirectionsRenderer for display.
   

    directionsService.route(request, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
      	directions = directionsRenderer;
        directionsRenderer.setDirections(result);
      } else {
        window.console.log('failed to get directions: '  + status);
      }
    });
};


function initialize() {
	
  var mapOptions = {
    zoom: 6
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

    var buttons = document.getElementById("search");
  	var directionbutt = document.getElementById("directions");  
  	directionbutt.addEventListener('click', makeDirection, true);
	buttons.addEventListener('click', findPlace, true);
	
	var search = document.getElementById('search_text');
	search.addEventListener('change', findPlace, true);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

      var infowindow = new google.maps.InfoWindow({
        map: map,
        position: pos,
        content: 'You are here!'
      });

      map.setCenter(pos);
      myPlace = pos;
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }

 }

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  
  map.setCenter(options.position);

  
}
google.maps.event.addDomListener(window, 'load', initialize);


