class CreateFriendRequests < ActiveRecord::Migration
  def change
    create_table :friend_requests do |t|
      t.text :message

      t.belongs_to :sender, :class_name => "User"
      t.belongs_to :receiver, :class_name => "User"

      t.timestamps
    end
  end
end
