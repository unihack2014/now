class AddEmailToFriendRequest < ActiveRecord::Migration
  def change
  	change_table :friend_requests do |t|
    	t.text :email
    end
  end
end
