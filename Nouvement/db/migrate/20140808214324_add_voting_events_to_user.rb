class AddVotingEventsToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
    	t.belongs_to :voting_event , class_name: 'Event'
    end
  end
end
