class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :date
      t.time :time
      t.text :name
      t.text :message
      t.text :location
      t.integer :state

      t.belongs_to :organiser, :class_name => "User"

      t.timestamps
    end
  end
end
