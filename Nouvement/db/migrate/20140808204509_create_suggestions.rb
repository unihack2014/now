class CreateSuggestions < ActiveRecord::Migration
  def change
    create_table :suggestions do |t|
      t.integer :num_votes

      t.belongs_to :event
      t.belongs_to :location
      t.timestamps
    end
  end
end
