class CreateCheckIns < ActiveRecord::Migration
  def change
    create_table :check_ins do |t|
      t.date :date
      t.time :time
      t.text :message
      
      t.belongs_to :location
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
