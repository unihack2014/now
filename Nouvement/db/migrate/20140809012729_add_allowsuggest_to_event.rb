class AddAllowsuggestToEvent < ActiveRecord::Migration
  def change
  	change_table :events do |t|
    	t.integer :allowsuggest
    end
  end
end
