class AcceptedUsersEvents < ActiveRecord::Migration
  def change
  	create_table :accepted_users_events, id: false do |t|
      t.belongs_to :user
      t.belongs_to :event
    end
  end
end
