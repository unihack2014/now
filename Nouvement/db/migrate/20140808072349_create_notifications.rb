class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :content
      t.integer :readstate

      t.belongs_to :user
      t.belongs_to :event
      t.timestamps
    end
  end
end
