class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.text :name
      t.integer :longitude
      t.integer :latitude

      t.belongs_to :final_event, :class_name => "Event"
      t.belongs_to :draft_event, :class_name => "Event"
      
      t.timestamps
    end
  end
end
