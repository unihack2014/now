class RejectedUsersEvents < ActiveRecord::Migration
  def change
  	create_table :rejected_users_events, id: false do |t|
      t.belongs_to :user
      t.belongs_to :event
    end
  end
end
