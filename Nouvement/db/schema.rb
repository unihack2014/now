# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140809035745) do

  create_table "accepted_users_events", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "event_id"
  end

  create_table "check_ins", force: true do |t|
    t.date     "date"
    t.time     "time"
    t.text     "message"
    t.integer  "location_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.date     "date"
    t.time     "time"
    t.text     "name"
    t.text     "message"
    t.text     "location"
    t.integer  "state"
    t.integer  "organiser_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "allowsuggest"
  end

  create_table "friend_requests", force: true do |t|
    t.text     "message"
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "email"
  end

  create_table "invited_users_events", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "event_id"
  end

  create_table "locations", force: true do |t|
    t.text     "name"
    t.integer  "longitude"
    t.integer  "latitude"
    t.integer  "final_event_id"
    t.integer  "draft_event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.text     "content"
    t.integer  "readstate"
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rejected_users_events", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "event_id"
  end

  create_table "suggestions", force: true do |t|
    t.integer  "num_votes"
    t.integer  "event_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "friend_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "voting_event_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
