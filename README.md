Summary
Nouvement is a Web App developed for the Unihack Melbourne competition.
Nouvement allows users a very way of creating and managing their events. The main interface is a map, where users can see all their events as pins.

Users are able to view details of the events by clicking on the pins. 

Set Up
Go to the Nouvement directory
Start rails server (rails s)
Using your favorite browser, go to the ip address specified by rails

Note 1: Application was developed and tested using Ruby 2.1.0 and Rails 4.1.4
Note 2: Unfortunately due to time limitations, the Web App was not completed